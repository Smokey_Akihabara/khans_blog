from django.contrib import admin
from blogs.models import Post,Nav,Comment,Category
# Register your models here.
admin.site.register(Post)
admin.site.register(Nav)
admin.site.register(Comment)
admin.site.register(Category)