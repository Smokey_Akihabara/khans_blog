from django.db import models


# Create your models here.

class Category(models.Model):
    name=models.CharField(max_length=255)
    status = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.name}"

class Post(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    view_count = models.IntegerField(default=0)
    category = models.ForeignKey(Category,on_delete=models.CASCADE, related_name='posts', null=True)
    def __str__(self):
        return f"{self.title}"


class Nav(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    links = models.CharField(max_length=255, null=True)
    def __str__(self):
        return f"{self.title}"


class Comment(models.Model):
    choice = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5)
    )
    name = models.CharField(max_length=255)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments', null=True)
    text = models.TextField()
    rate = models.IntegerField(choices=choice, default=0)
    def __str__(self):
        return f"{self.name}"
