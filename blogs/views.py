from django.shortcuts import render, redirect
from blogs.models import Post, Nav, Comment, Category
from django.db.models import Avg


# Create your views here.

def blogs(request):
    navs = Nav.objects.all()
    categorys = Category.objects.filter(status=True).all()
    category = request.GET.get("category")
    if category:
        posts = Post.objects.filter(category_id=category).all()
    else:
        posts = Post.objects.all()
    return render(request=request, template_name="tech-index.html",
                  context={"posts": posts, "navs": navs, "categorys": categorys})


def blog(request):
    post = Post.objects.filter(id=request.GET.get("id")).first()
    comments = Comment.objects.filter(post=post).order_by("-id").all()
    avarage = Comment.objects.filter(post=post).aggregate(Avg("rate"))

    if post:
        post.view_count = post.view_count + 1
        post.save()
    return render(request=request, template_name="tech-single.html",
                  context={"post": post, "comments": comments, "avarage": avarage})


def save(request):
    post_id = request.POST.get("post_id")
    name = request.POST.get("name")
    text = request.POST.get("text")
    category = request.GET.get("category")
    rate = request.POST.get("rate", 3)
    Comment.objects.create(
        name=name,
        post_id=post_id,
        text=text,
        rate=rate,
    ).save()
    return redirect("/post?id=" + post_id)


def postsave(request):
    title = request.POST.get("title")
    text = request.POST.get("text")
    category = request.POST.get("category")
    Post.objects.create(
        title=title,
        text=text,
        category_id=category,
    ).save()
    return redirect("/posts")


def postupdate(request):
    post_id = request.GET.get("id")
    post = Post.objects.filter(id=post_id).first()
    categorys = Category.objects.filter(status=True).all()
    return render(request=request, template_name="postupdate.html", context={"post": post, "categorys": categorys})


def poststore(request):
    post_id = request.POST.get("id")
    title = request.POST.get("title")
    text = request.POST.get("text")
    category = request.POST.get("category")
    post = Post.objects.filter(id=post_id).first()
    if post:
        post.title = title
        post.text = text
        post.category_id = category
        post.save()
    return redirect("/post?id=" + post_id)


def create(request):
    categorys = Category.objects.all()
    return render(request=request, template_name="ckeditor.html", context={"categorys": categorys})


def navs(request):
    return render(request=request, template_name="tech-index.html", context={"navs": navs})


def nav(request):
    nav = Nav.objects.filter(id=request.GET.get("id")).first()
    return render(request=request, template_name="tech-index.html", context={"nav": nav})
