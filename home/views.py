from django.shortcuts import render
from home.models import post

def list(request):
    return render(request=request, template_name="tech-index.html")

def single(request):
    return render(request=request, template_name="tech-single.html")

def contact(request):
    return render(request=request, template_name="tech-contact.html")
